package com.warathip.week12;

import java.util.ArrayList;
import java.util.Collections;

public class TestArrayList {
    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<String>();
        list.add("A1");
        list.add("A2");
        list.add("A3");

        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }

        list.add(1, "A0");
        list.add("A3");
        for(String str:list) {
            System.out.println(str);
        }
        String[] arr = list.toArray(new String[list.size()]);
        for(int i=0; i<arr.length; i++) {
            System.out.println(arr[i]);
        }

        System.out.println();
        list.remove("A0");
        System.out.println(list);
        list.remove(3);
        System.out.println(list);
        list.set(0, "A5");
        System.out.println(list);

        ArrayList<String> list2 = new ArrayList<>(list);
        System.out.println(list2);
        list2.add("A4");
        System.out.println(list2);
        list.addAll(list2);
        System.out.println(list);

        System.out.println(list2.contains("A3"));
        System.out.println(list2.indexOf("A5"));
        Collections.sort(list);
        System.out.println(list);
        Collections.shuffle(list);
        System.out.println(list);
        Collections.reverse(list);
        System.out.println(list);
        Collections.swap(list, 0, 2);
        System.out.println(list);
    }
}
